FROM python:3.10-slim

WORKDIR /app
COPY requirements/requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt
COPY src/ /app/src
CMD ["python", "src/main.py"]

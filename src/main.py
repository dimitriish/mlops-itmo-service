from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import mlflow.pyfunc
import pandas as pd
import numpy as np
import torch
from dotenv import load_dotenv

load_dotenv()

app = FastAPI()

class PredictionRequest(BaseModel):
    data: list

model_name = "ffn"
model_version = 3

try:
    model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_version}")
    print("Model loaded successfully.")
except Exception as e:
    print(f"Error loading model: {str(e)}")
    model = None

@app.post("/predict")
async def predict(request: PredictionRequest):
    if model is None:
        raise HTTPException(status_code=500, detail="Model is not available")

    try:
        data = pd.DataFrame(request.data)
        predictions = model.predict(data)

        if isinstance(predictions, torch.Tensor):
            predictions = predictions.numpy().tolist()
        elif isinstance(predictions, np.ndarray):
            predictions = predictions.tolist()

        return {"predictions": predictions}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/")
async def read_root():
    return {"message": "Welcome to the model serving API"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
